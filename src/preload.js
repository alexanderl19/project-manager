const { contextBridge, ipcRenderer } = require("electron");

contextBridge.exposeInMainWorld("ipcRenderer", {
    reinitializeProjectManager: () =>
        ipcRenderer.invoke("reinitializeProjectManager"),
    addProject: (folderName, projectObject) =>
        ipcRenderer.invoke("addProject", folderName, projectObject),
    getIndex: () => ipcRenderer.invoke("getIndex"),
    selectIndex: () => ipcRenderer.invoke("selectIndex"),
    setIndex: (path) => ipcRenderer.invoke("setIndex", path),
    minimize: () => ipcRenderer.invoke("minimize"),
    unmaximize: () => ipcRenderer.invoke("unmaximize"),
    maximize: () => ipcRenderer.invoke("maximize"),
    close: () => ipcRenderer.invoke("close"),
    maximizeListener: (listener) => {
        ipcRenderer.on("maximize", listener);
    },
    maximizeAllListenerRemove: () => ipcRenderer.removeAllListeners("maximize"),
    unmaximizeListener: (listener) => ipcRenderer.on("unmaximize", listener),
    unmaximizeAllListenerRemove: () =>
        ipcRenderer.removeAllListeners("unmaximize"),
    indexUpdatedListener: (listener) =>
        ipcRenderer.on("indexUpdated", listener),
    indexUpdatedListenerRemove: () =>
        ipcRenderer.removeAllListeners("indexUpdated"),
    currentVersion: () => ipcRenderer.invoke("currentVersion"),
    checkForUpdates: () => ipcRenderer.invoke("checkForUpdates"),
    installUpdate: () => ipcRenderer.invoke("installUpdate"),
    updateAvailableListener: (listener) =>
        ipcRenderer.on("updateAvailable", listener),
    updateAvailableListenerRemove: () =>
        ipcRenderer.removeAllListeners("updateAvailable"),
    downloadProgressListener: (listener) =>
        ipcRenderer.on("downloadProgress", listener),
    downloadProgressListenerRemove: () =>
        ipcRenderer.removeAllListeners("downloadProgress"),
    updateDownloadedListener: (listener) =>
        ipcRenderer.on("updateDownloaded", listener),
    updateDownloadedListenerRemove: () =>
        ipcRenderer.removeAllListeners("updateDownloaded"),
    openProject: (path) => ipcRenderer.invoke("openProject", path),
});
