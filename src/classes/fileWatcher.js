import chokidar from "chokidar";
import { IndexManager } from "./indexManager";
import { promises as fsPromises } from "fs";
import EventEmitter from "events";
class FileWatcherEmitter extends EventEmitter {}
import path from "path";

class FileWatcher {
    #path;
    #indexManager;
    #watcher;
    #eventEmitter;

    constructor(path) {
        this.#eventEmitter = new FileWatcherEmitter();
        this.#path = path;
        this.#indexManager = new IndexManager(path);

        this.#watcher = chokidar.watch(path, {
            ignored: /(^|[/\\])\../,
            depth: 0,
            persistent: true,
        });

        this.#watcher
            .on("addDir", (path) => this.addDirectory(path))
            .on("unlinkDir", (path) => this.unlinkDirectory(path));
    }

    //getIndex function is here so it can be accessed from Project Manager
    getIndex() {
        return this.#indexManager.getIndex();
    }

    getFileWatcherEmitter() {
        return this.#eventEmitter;
    }

    getProjectObject(folderPath) {
        return new Promise((resolve, reject) => {
            fsPromises
                .readFile(path.join(folderPath, "project.json"), "utf8")
                .then((r) => {
                    resolve(JSON.parse(r));
                })
                .catch((e) => {
                    reject(e);
                });
        });
    }

    emitUpdate() {
        this.#eventEmitter.emit("indexUpdated", this.getIndex());
    }

    async addDirectory(path) {
        if (path === this.#path) return;
        this.getProjectObject(path)
            .then((r) => {
                this.#indexManager.addProject(path, r);
            })
            //Catches error when reading project.json (folder has no project.json)
            .catch(() => {})
            .then(() => {
                this.emitUpdate();
            });
    }

    unlinkDirectory(path) {
        this.#indexManager
            .removeProject(path)
            //Index probably could not be persisted
            .catch(() => {});
        this.emitUpdate();
    }
}

export { FileWatcher };
