import { promises as fsPromises } from "fs";
import path from "path";

class IndexManager {
    #index = [];
    #basePath;

    constructor(basePath) {
        this.#basePath = basePath;
    }

    getIndex() {
        return this.#index;
    }

    persistIndex() {
        fsPromises
            .writeFile(
                path.join(this.#basePath, "index.json"),
                JSON.stringify(this.#index)
            )
            .catch((e) => {
                throw e;
            });
    }

    getFolderName(fullPath) {
        const folderName = fullPath.split(path.sep);
        return folderName[folderName.length - 1];
    }

    sortIndex() {
        this.#index.sort((projectOne, projectTwo) => {
            return projectTwo.created - projectOne.created;
        });
    }

    addProject(path, projectObject) {
        const projectObjectWithFolderPath = projectObject;
        projectObjectWithFolderPath.folder_name = this.getFolderName(path);
        this.#index.push(projectObjectWithFolderPath);
        this.sortIndex();
        this.persistIndex();
    }

    //This method is async so the .catch syntax can be used
    async removeProject(path) {
        const folderName = this.getFolderName(path);
        const index = this.#index.findIndex((project) => {
            return project.folder_name === folderName;
        });
        if (!index) return;
        this.#index.splice(index, 1);
        this.sortIndex();
        this.persistIndex();
    }
}

export { IndexManager };
