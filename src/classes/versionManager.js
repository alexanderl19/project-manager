import { win } from "@/ipcMain";
import { ipcMain } from "electron";
import { autoUpdater } from "electron-updater";

autoUpdater.setFeedURL({
    provider: "generic",
    url:
        "https://gitlab.com/alexanderl19/project-manager/-/jobs/artifacts/master/raw/dist_electron?job=publish",
});

autoUpdater.checkForUpdates();

setInterval(() => {
    autoUpdater.checkForUpdates();
}, 15 * 60 * 1000);

ipcMain.handle("currentVersion", async () => {
    return autoUpdater.currentVersion.version;
});

ipcMain.handle("checkForUpdates", async () => {
    await autoUpdater.checkForUpdates();
});

ipcMain.handle("installUpdate", async () => {
    autoUpdater.quitAndInstall();
});

autoUpdater.on("update-available", (info) => {
    win.webContents.send("updateAvailable", info);
});

autoUpdater.on("download-progress", (progressObj) => {
    win.webContents.send("downloadProgress", progressObj);
});

autoUpdater.on("update-downloaded", (info) => {
    win.webContents.send("updateDownloaded", info);
});
