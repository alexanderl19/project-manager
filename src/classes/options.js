import fs from "fs";
import { promises as fsPromises } from "fs";
import path from "path";

class Options {
    #userData;
    #name;
    #store;
    //0: not initialized
    //1: finished initialization
    //2: failed initialization
    #state = 0;

    constructor(userData, fileName) {
        this.#name = fileName;
        this.#userData = userData;
        fsPromises
            .access(path.join(this.#userData, this.#name), fs.constants.R_OK)
            .then(() => {
                this.readFile();
            })
            .catch(async () => {
                await this.createFile(fileName);
                this.readFile();
            });
    }

    readFile() {
        fsPromises
            .readFile(path.join(this.#userData, this.#name), "utf8")
            .then((r) => {
                this.#store = JSON.parse(r);
                this.#state = 1;
            })
            .catch((e) => {
                this.#state = 2;
                throw e;
            });
    }

    informInitialized() {
        return new Promise((resolve, reject) => {
            const interval = setInterval(() => {
                if (this.#state === 1) {
                    clearInterval(interval);
                    resolve();
                } else if (this.#state === 2) {
                    clearInterval(interval);
                    reject("State failed to initialize.");
                }
            }, 100);
        });
    }

    getParameter(parameterName) {
        if (!this.#store) {
            throw new Error("Store is not initialized.");
        }
        return this.#store[parameterName];
    }

    setParameter(parameterName, value) {
        if (!this.#store) {
            throw new Error("Store is not initialized.");
        }
        this.#store[parameterName] = value;
        this.persistStore(this.#store).catch(() => {
            throw new Error("Store could not be persisted.");
        });
    }

    persistStore(store) {
        return new Promise((resolve, reject) => {
            fsPromises
                .writeFile(
                    path.join(this.#userData, this.#name),
                    JSON.stringify(store),
                    "utf8"
                )
                .then(() => {
                    resolve();
                })
                .catch((e) => {
                    reject(e);
                });
        });
    }

    createFile(fileName) {
        fsPromises
            .writeFile(path.join(this.#userData, fileName), "{}")
            .catch((e) => {
                throw e;
            });
    }
}

export { Options };
