import { promises as fsPromises } from "fs";
import { FileWatcher } from "./fileWatcher";
import path from "path";
import Ajv from "ajv";

//Handles projects for one location
class ProjectManager {
    #fileWatcher;
    #basePath;
    #validateProject;
    #filenameRegex = /^(?!.*[\\/?%*:|"<>])(?!.*CON|PRN|AUX|CLOCK\$|NUL|COM1|COM2|COM3|COM4|COM5|COM6|COM7|COM8|COM9|LPT1|LPT2|LPT3|LPT4|LPT5|LPT6|LPT7|LPT8|LPT9)/;

    constructor(basePath) {
        this.#basePath = path.normalize(basePath);
        this.#fileWatcher = new FileWatcher(this.#basePath);
        this.#validateProject = new Ajv().compile(
            require("@/schema/project.schema.json")
        );
    }

    getIndex() {
        return this.#fileWatcher.getIndex();
    }

    getFileWatcherEmitter() {
        return this.#fileWatcher.getFileWatcherEmitter();
    }

    //Testing folderName parameter because it comes from renderer process
    validateProjectAndFolder(folderName, projectObject) {
        return (
            this.#validateProject(projectObject) &&
            this.#filenameRegex.test(folderName)
        );
    }

    writeProjectOptions(folderName, projectObject) {
        fsPromises
            .writeFile(
                path.join(this.#basePath, folderName, "project.json"),
                JSON.stringify(projectObject)
            )
            .catch((e) => {
                throw e;
            });
    }

    addProject(folderName, projectObject) {
        return new Promise((resolve, reject) => {
            if (!this.validateProjectAndFolder(folderName, projectObject))
                reject("Invalid project or folder name.");
            fsPromises
                .mkdir(path.join(this.#basePath, folderName))
                .catch((e) => {
                    reject(e);
                })
                .then(() => {
                    this.writeProjectOptions(folderName, projectObject);
                })
                .then(() => {
                    resolve();
                });
        });
    }

    editProject(folderName, projectObject) {
        if (!this.validateProjectAndFolder(folderName, projectObject))
            throw new Error("Invalid project or folder name.");
        this.writeProjectOptions(folderName, projectObject);
    }
}

export { ProjectManager };
