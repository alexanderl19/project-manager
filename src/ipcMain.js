import { app, ipcMain, shell, dialog } from "electron";
import { ProjectManager } from "@/classes/projectManager";
import { Options } from "@/classes/options";
import { win } from "@/background";
import path from "path";
import { createProtocol } from "vue-cli-plugin-electron-builder/lib";

export { win };

require("@/classes/versionManager");

const userData = app.getPath("userData");

let basePath, projectManager, FileWatcherEmitter;

require("@/ipcTitleBar");

ipcMain.handle("selectIndex", async () => {
    return dialog.showOpenDialog({
        defaultPath: app.getPath("desktop"),
        properties: ["openDirectory"],
    });
});

const configuration = new Options(userData, "config.json");

ipcMain.handle("setIndex", async (e, path) => {
    configuration.setParameter("baseFolder", path);
});

configuration.informInitialized().then(() => {
    basePath = configuration.getParameter("baseFolder");
    if (!basePath) {
        if (process.env.WEBPACK_DEV_SERVER_URL) {
            // Load the url of the dev server if in development mode
            win.loadURL(process.env.WEBPACK_DEV_SERVER_URL + "/#/setup");
        } else {
            createProtocol("app");
            // Load the index.html when not in development
            win.loadURL("app://./index.html/#/setup");
        }
    }

    const initializeProjectManager = (basePath) => {
        projectManager = new ProjectManager(basePath);
        FileWatcherEmitter = projectManager.getFileWatcherEmitter();
        FileWatcherEmitter.on("indexUpdated", (index) => {
            win.webContents.send("indexUpdated", index);
        });
    };

    try {
        initializeProjectManager(basePath);
    } catch (e) {
        console.error(e);
    }

    ipcMain.handle("reinitializeProjectManager", async () => {
        basePath = configuration.getParameter("baseFolder");
        initializeProjectManager(basePath);
    });

    ipcMain.handle("addProject", async (e, folderName, projectObject) => {
        return projectManager
            .addProject(folderName, projectObject)
            .then(() => {
                return "success";
            })
            .catch(() => {
                return new Error("Could not add project.");
            });
    });

    ipcMain.handle("getIndex", async () => {
        return projectManager.getIndex();
    });

    ipcMain.handle("openProject", async (e, relativePath) => {
        return shell.openPath(path.join(basePath, relativePath));
    });
});
