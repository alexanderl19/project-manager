import { win } from "@/ipcMain";
import { ipcMain } from "electron";

win.on("maximize", () => {
    win.webContents.send("maximize");
});

win.on("unmaximize", () => {
    win.webContents.send("unmaximize");
});

ipcMain.handle("minimize", async () => {
    win.minimize();
});

ipcMain.handle("unmaximize", async () => {
    win.unmaximize();
});

ipcMain.handle("maximize", async () => {
    win.maximize();
});

ipcMain.handle("close", async () => {
    win.close();
});
