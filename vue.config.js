const path = require("path");

module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/scss/global.scss";`,
            },
        },
    },
    pluginOptions: {
        electronBuilder: {
            preload: "src/preload.js",
            // Babel config for background.js
            // Separate from babel config for Vue
            chainWebpackMainProcess: (config) => {
                config.module
                    .rule("babel")
                    .test(/\.js$/)
                    .include.add(path.resolve(__dirname, "src", "classes"))
                    .end()
                    .use("babel")
                    .loader("babel-loader")
                    .options({
                        presets: [["@babel/preset-env", { modules: false }]],
                        plugins: ["@babel/plugin-proposal-class-properties"],
                    });
            },
            builderOptions: {
                publish: [
                    {
                        provider: "github",
                        owner: "alexanderl19",
                        repo: "project-manager",
                    },
                ],
            },
        },
    },
};
