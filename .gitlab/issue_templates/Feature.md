<!--- Provide a general summary of the feature in the Title above -->

## Desired Behavior
<!--- Tell us what should happen -->

## Reasoning
<!-- Is your feature request related to a problem? -->
<!-- Please describe the problem you are trying to solve. -->

<!--- Provide a general summary of the feature in the Title above -->

## Detailed Description
<!--- Provide a detailed description of the change or addition you are proposing -->

## Possible Implementation
<!--- Not obligatory, but suggest an idea for implementing addition or change -->
